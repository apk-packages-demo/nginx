# nginx

* Scripts based on other images than "alpine" are in [gitlab.com/hub-docker-com-demo/nginx](https://gitlab.com/hub-docker-com-demo/nginx)

## TODO
### UNIX Socket
#### Nginx
##### Keyworks
* [nginx server listen unix socket](https://www.google.com/search?q=nginx+server+listen+unix+socket)

##### Official documentation
* server listen [*Server names*](http://nginx.org/en/docs/http/server_names.html)
* [*Configuring Logging*](https://docs.nginx.com/nginx/admin-guide/monitoring/logging/)

##### Contributed documentation
* [*Proxy to nginx unix socket*](https://caddy.community/t/proxy-to-nginx-unix-socket/1759)

#### cURL
* [curl unix socket](https://www.google.com/search?q=curl+unix+socket)
* [*Can cURL send requests to sockets?*](https://superuser.com/questions/834307/can-curl-send-requests-to-sockets)

## Alpine packages demo used
* [procps](https://gitlab.com/alpinelinux-packages-demo/procps)
